# Hill function fitting: parameter fitting for hill function in DTW metric

- Author: Bansho Masutani<banmasutani@gmail.com>
- Language: Rust
- date: 2017-11-05

## Aim

+ To determine the parameters of Hill function.

## What for

+ Sub-DTW used in "event-stream" comparison in selective sequencing has a element-wize function to compute a "distance" between each position between a reference and a query.
+ In analogy with semi-global alignment in sequence comparison, a function which is sigmoidal seems to be good in real application: Hill function(x,y|a,b)  = d / (b + d) where d = (x-y)^a.

## Method

+ Automated grid search.
+ The range of a is firstly set to 1-10, b 0-1.
+ After rought sketch of the "landscape" of parameter space, I choose alpha = 2 and futher tuning beat.
+ Queries which predicted to be mapped onto the template strand by minimap2 was used as a query. From them, I get a positive score by mapping it to template, negative score by mapping it to revcomp.
+ 5 fold CV
+ 15-NN is used as a downstream classifier.

## Result

+ alpha = 2,beta = 1.
+ This is also efficient in computational time(power of real value is slower than integer one).