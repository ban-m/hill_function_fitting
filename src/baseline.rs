extern crate dtw;
extern crate knn_predictor;
extern crate fast5wrapper;
extern crate rand;
extern crate squiggler;
extern crate bio;
extern crate rayon;
extern crate utility;
use std::path::Path;
use rayon::prelude::*;
use utility::utilities;
fn main() {
    let args :Vec<_>  = std::env::args().collect();
    let model = squiggler::Squiggler::new(&Path::new(&args[1])).expect("model");
    let (temp,rev) = utilities::setup_template_complement(&Path::new(&args[2])).expect("template");
    let (temp,rev) = (utilities::convert_to_squiggle(&temp,&model),
                      utilities::convert_to_squiggle(&rev,&model));
    let refsize :usize = args[3].parse().expect("refsize");
    let refsize = refsize*1000;
    let querysize:usize = args[4].parse().expect("querysize");
    let querynum:usize = args[5].parse().expect("querynum");
    let queries = utilities::get_queries(&args[6],querysize,querynum*2).expect("query");
    let sam = utilities::get_sam(&args[7]).expect("sam");
    let queries = utilities::merge_queries_and_sam(&queries,&sam);
    let mode = dtw::Mode::Sub;
    let dataset:Vec<_> = queries.par_iter()
        .filter_map(|&(ref query,flag,location)|
                    if flag == 0 {
                        Some((query,flag,location))
                    }else if flag == 16{
                        Some((query,flag,if rev.len()> location + refsize{rev.len()-location-refsize}else{0}))
                    }else {
                        None
                    })
        .filter_map(|(query,flag,location)|
                    if flag == 0 {
                        Some((utilities::get_score(&query,location,&temp,
                                                   refsize,querysize,&mode,0.,"normal"),
                              utilities::get_score(&query,location,&rev,
                                                   refsize,querysize,&mode,0.,"normal")))
                    }else if flag == 16 {
                        Some((utilities::get_score(&query,location,&rev,
                                                   refsize,querysize,&mode,0.,"normal",),
                              utilities::get_score(&query,location,&temp,
                                                   refsize,querysize,&mode,0.,"normal")))
                    }else{
                        None
                    })
        .filter_map(|(e,f)|
                    if let Some(pos) = e {
                        if let Some(neg) = f {
                            Some((pos,neg))
                        }else{
                            None
                        }
                    }else{
                        None
                    })
        .collect();
    let dataset:Vec<_> = dataset.into_iter().take(querynum).collect();
    eprintln!("{}",dataset.len());
    let (true_positive,false_positive,true_num,total_num) =
        utilities::validate(&dataset,"KNN",5);
    println!("{},{},{},{}",true_positive,false_positive,true_num,total_num);
}

