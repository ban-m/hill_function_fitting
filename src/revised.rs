extern crate dtw;
extern crate knn_predictor;
extern crate fast5wrapper;
extern crate rand;
extern crate squiggler;
extern crate bio;
extern crate rayon;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;
use rand::thread_rng;
use rand::Rng;
use rayon::prelude::*;
use knn_predictor::knn::KNN;
const K:usize = 10;
fn main() {
    let args :Vec<_>  = std::env::args().collect();
    let model = squiggler::Squiggler::new(&Path::new(&args[1])).expect("model");
    let (temp,rev) = setup_template_complement(&Path::new(&args[2])).expect("template");
    let temp:Vec<_> = dtw::normalize(&model.get_signal_from_fasta(&temp)
                                     .into_iter().map(|e| e.2).collect()
                                     ,dtw::NormalizeType::Z);
    let rev:Vec<_> = dtw::normalize(&model.get_signal_from_fasta(&rev)
                                    .into_iter().map(|e| e.2).collect()
                                    ,dtw::NormalizeType::Z);
    let refsize :usize = args[3].parse().expect("refsize");
    let refsize = refsize*1000;
    let querysize:usize = args[4].parse().expect("querysize");
    let querynum:usize = args[5].parse().expect("querynum");
    let queries:Vec<_> = BufReader::new(File::open(&Path::new(&args[6])).expect("file"))
        .lines().filter_map(|e|e.ok()).filter_map(|e|{
            let contents :Vec<_> = e.split(',').collect();
            let query = match fast5wrapper::get_event_for(contents[0],50,querysize){
                Ok(res) => dtw::normalize(&res.into_iter().map(|e|e[2]).collect(),dtw::NormalizeType::Z),
                Err(why)=>{eprintln!("{:?}",why);return None},
            };
            let location:usize = contents[1].parse().expect("parse");
            if location < 100 || location + refsize - 100 > temp.len(){
                return None
            }
            Some((query,location))})
        .take(querynum).collect();
    let n_max:i32 = args[7].parse().expect("nmax");
    let alpha_max:f32 = args[8].parse().expect("alphamax");
    let num_alpha:usize = args[9].parse().expect("numalpha");
    let alpha_min:f32 = 0.2;
    let d_alpha = (alpha_max - alpha_min) / num_alpha as f32;
    let alphas:Vec<f32>= (0..num_alpha)
        .map(|n| alpha_min + d_alpha * n as f32)
        .collect();
    let ns:Vec<_> = (1..n_max+1).collect();
    let res = cross_validation(&queries,
                               &temp,
                               &rev,
                               refsize,
                               &ns,&alphas);
    for (score,n,a) in res{
        println!("{},{},{}",score,n,a);
    }
}

fn setup_template_complement(path:&Path)->std::result::Result<(String,String),()>{
    let seq = bio::io::fasta::Reader::from_file(path).map_err(|_|())?;
    let reference:Vec<u8> = seq.records()
        .filter_map(|e|e.ok())
        .fold(Vec::new(),|mut acc,e|{acc.extend_from_slice(&mut e.seq());acc});
    let reverse = bio::alphabets::dna::revcomp(&reference);
    let temp = String::from_utf8(reference).map_err(|_|())?;
    let rev = String::from_utf8(reverse).map_err(|_|())?;
    Ok((temp,rev))
}

fn cross_validation(queries:&Vec<(Vec<f32>,usize)>,temp:&Vec<f32>,rev:&Vec<f32>,refsize:usize,
                    ns:&Vec<i32>,alphas:&Vec<f32>) 
                    -> Vec<(f32,i32,f32)>{
    // cross varidate k-NN for given dataset.return each score,n,a
    let parameters = ns.iter().map(|&n| alphas.iter().map(|&alpha|(n,alpha)).collect())
        .fold(vec![],|mut acc,mut x|{acc.append(&mut x);acc});
    parameters.iter().map(|&(n,alpha)|{
        let dataset = get_dataset(queries,temp,rev,refsize,n,alpha);
        eprintln!("dataset length{}",dataset.len());
        (varidate(&dataset),n,alpha)
    }).collect::<Vec<(f32,i32,f32)>>()
}

fn get_dataset(queries:&Vec<(Vec<f32>,usize)>,temp:&Vec<f32>,rev:&Vec<f32>,refsize:usize,
              n:i32,alpha:f32)->Vec<(f64,bool)>{
    eprintln!("get dataset from parameter n:{},alpha:{}",n,alpha);
    let len = queries.len();
    let mut correct :Vec<_>= queries[0..len/2].par_iter()
        .filter_map(|&(ref query,location)|get_score(&query,location,temp,refsize,n,alpha))
        .map(|e|(e,true))
        .collect();
    let mut uncorrect:Vec<_> = queries[len/2..len].par_iter()
        .filter_map(|&(ref query,location)|get_score(&query,location,rev,refsize,n,alpha))
        .map(|e|(e,false))
        .collect();
    correct.append(&mut uncorrect);
    let mut rng = thread_rng();
    rng.shuffle(&mut correct);
    correct
}

fn get_score(query:&Vec<f32>,location:usize,reference:&Vec<f32>,refsize:usize,
             n:i32,alpha:f32)->Option<f64>{
    let hill = move |x:&f32,y:&f32|{
        let d = (x-y).powi(2*n);
        d/(alpha + d)
    };
//    let normal = |x:&f32,y:&f32|(x-y).powi(2);
    if location < 100 || location + refsize - 100 > reference.len(){
        return None
    }else{
        let result = dtw::dtw(query,&reference[location-100..location+refsize-100],dtw::Mode::Sub,&hill)
            .map(|e|e.0 as f64).ok();
        if let Some(x) = result{
            if x.is_infinite(){
                eprintln!("{:?}\n{:?}",&query[0..100],&reference[0..100]);
                assert!(false,"n:{},alpha:{},refsize:{},location{}",alpha,n,refsize,location)
            }
        }
        result
    }
}

fn varidate(dataset:&Vec<(f64,bool)>) -> f32{
    // varidate k(parameter)-NN by K-Folds cross varidation;
    // return the average number of correctly distinguished data in each "fold".
    compute_k_folds(dataset).iter().map(|&(ref train,ref test)|{
//        eprintln!("train:{},test:{}",train.len(),test.len());
        let predictor = KNN::new(&train,15);//15-NN
        predictor.predict_test(&test) as f32/test.len() as f32})
        .sum::<f32>() / K as f32
}

fn compute_k_folds(dataset:&Vec<(f64,bool)>) ->Vec<(Vec<(f64,bool)>,Vec<(f64,bool)>)>{
    let window_size = dataset.len()/K;
    let mut result = std::vec::Vec::with_capacity(K);
    for i in 0..(K-1){
        let testset:Vec<_> = dataset[i*window_size..(i+1)*window_size].iter().map(|e|e.clone()).collect();
        let trainset:Vec<_> = dataset.iter().enumerate()
            .filter(|&(idx,_)| idx < i*window_size || idx >= (i+1)*window_size)
            .map(|(_,&e)|e.clone()).collect();
        result.push((trainset,testset));
    }
    let testset:Vec<_> = dataset[(K-1)*window_size..].iter().map(|e|e.clone()).collect();
    let trainset:Vec<_> = dataset[..(K-1)*window_size].iter().map(|e|e.clone()).collect();
    result.push((trainset,testset));
    result
}


fn next_parameter(result:&Vec<(f32,i32,f32)>)->(i32,f32){
    // find maximum
    let opt = result.iter().fold((0.,0,0.),|acc,x| if acc.0 > x.0 { acc } else{ x.clone() });
    (opt.1,opt.2)
}
