library("tidyverse")
setwd("~/work/hill_function_fitting/")

generalplot <- function(g,name){
    pdf(paste0("./pdf/",name,".pdf"))
    plot(g)
    dev.off()
    png(paste0("./png/",name,".png"),width = 480*2,height = 480*2)
    plot(g)
    dev.off()
}

threshold <- function(score){
    ifelse(score < 0.8,0,(score-0.8)*15)
}


baseline <- read_csv("./result/baseline.csv") %>% mutate(accuracy = (test_num - false_negative + true_positive)/test_num)
rawdata <- read_csv("./result/hill_function_fitting.csv") %>%  mutate(accuracy = (test_num - false_negative + true_positive)/test_num)


g <- rawdata %>% ggplot(mapping = aes(x = n , y = alpha)) + geom_tile(mapping = aes(fill = accuracy)) +
    labs(title = "heatmap of hill function patameter",x = "beta",y = "alpha") + theme_bw()
generalplot(g,"result")
base_accuracy <- baseline %>% head(n=1) %>% pull(accuracy)

g <- rawdata %>% filter(n == 1) %>% ggplot(mapping = aes(x = alpha, y = accuracy)) + geom_line() +
    geom_hline(yintercept = base_accuracy) + theme_bw() +
    labs(x = "alpha",y="accuracy")
