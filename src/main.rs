extern crate dtw;
extern crate squiggler;
extern crate rayon;
extern crate utility;
use std::path::Path;
use rayon::prelude::*;
use utility::utilities;
fn main() {
    let args :Vec<_>  = std::env::args().collect();
    let model = squiggler::Squiggler::new(&Path::new(&args[1])).expect("model");
    let (temp,rev) = utilities::setup_template_complement(&Path::new(&args[2])).expect("template");
    let temp = utilities::convert_to_squiggle(&temp,&model);
    let rev = utilities::convert_to_squiggle(&rev,&model);
    let refsize :usize = args[3].parse().expect("refsize");
    let refsize = refsize*1000;
    let querysize:usize = args[4].parse().expect("querysize");
    let querynum:usize = args[5].parse().expect("querynum");
    let queries = utilities::get_queries(&args[6],querysize,querynum*2).expect("query");
    let sam = utilities::get_sam(&args[7]).expect("sam");
    let queries = utilities::merge_queries_and_sam(&queries,&sam);
    let mode = dtw::Mode::QuickSub;
    eprintln!("{}",queries.len());
    let max_n:usize = args[8].parse().expect("maxn");
    let max_a:f32 = args[9].parse().expect("maxa");
    let num_a:usize = args[10].parse().expect("numa");
    let ns:Vec<_> = (1..max_n+1).collect();
    let alphas:Vec<_> = (0..num_a).map(|e| 0.01 + max_a * e as f32 / num_a as f32).collect();
    eprintln!("started");
    for n in ns{
        for a in alphas.iter(){
            eprintln!("n:{},alpha:{}",n,a);
            let n = n as i32;
            let pow = n;
            let alpha = a;
            let dist = move |x:&f32,y:&f32| {
                let d = (x-y).powi(2*n);
                (d/(a + d))
            };
            let dataset:Vec<_> = queries.par_iter()
                .filter_map(|&(ref query,flag,location)|
                            if flag == 0 {
                                Some((query,flag,location))
                            }else if flag == 16{
                                Some((query,flag,
                                      if rev.len()> location + refsize{rev.len()-location-refsize}else{0}))
                            }else {
                                None
                            })
                .filter_map(|(query,flag,location)|{
                    let query = dtw::normalize(&query, dtw::NormalizeType::Z);
                    if query.len() < querysize {
                        return None;
                    };
                    let query: Vec<_> = query.into_iter().take(querysize).collect();
                    let offset = 500;
                    let (reference,neg_reference) = if flag == 0 {
                        (&temp,&rev)
                    }else if flag ==16 {
                        (&rev,&temp)
                    }else {
                        return None
                    };
                    let subref = if location < offset {
                        &reference[0..refsize]
                    } else if location + refsize - offset > reference.len() {
                        &reference[reference.len() - offset - refsize..]
                    } else {
                        &reference[location - offset..location - offset + refsize]
                    };
                    Some((dtw::dtw(&query,subref,mode.clone(),&dist).ok(),
                         dtw::dtw(&query,&neg_reference[0..refsize],mode.clone(),&dist).ok()))
                })
                .filter_map(|(e,f)|
                            if let Some(pos) = e {
                                if let Some(neg) = f {
                                    Some((pos.0 as f64,neg.0 as f64))
                                }else{
                                    None
                                }
                            }else{
                        None
                            })
                .collect();
            let dataset:Vec<_> = dataset.into_iter().take(querynum).collect();
            let (true_positive,false_positive,true_num,total_num) =
                utilities::validate(&dataset,"KNN",5);
            println!("{},{},{},{},{},{}",true_positive,false_positive,true_num,total_num,pow,alpha);
        }
    }
}
